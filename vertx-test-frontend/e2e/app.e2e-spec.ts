import { VertxTestFrontPage } from './app.po';

describe('vertx-test-front App', () => {
  let page: VertxTestFrontPage;

  beforeEach(() => {
    page = new VertxTestFrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
