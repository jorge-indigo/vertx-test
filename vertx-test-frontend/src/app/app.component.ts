import { Component } from '@angular/core';
import { User } from './domain/user';
import { UserService } from './services/user.service';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [UserService]
})
export class AppComponent {
  
  user: User;
  options: any;

  constructor(private userService: UserService, private toasterService: ToasterService) {
    this.user = new User();
    this.options = {}
  }

  submitForm() {
    this.userService.saveUser(this.user)
      .then(() => {
        if (!this.options.clear) { this.user = new User() }
        this.toasterService.pop('success', 'OK', 'Se guardó el usuario')
      })
      .catch(() => this.toasterService.pop('error', 'ERROR', 'No se pudo guardar el usuario'));
  }
}
