import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { User } from '../domain/user';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService {

    constructor(private $http: Http) { }

    saveUser(user: User) {
        return this.$http.post("http://localhost:8080/user", user)
            .toPromise();
    }
}