import io.vertx.groovy.ext.mongo.MongoClient

def config = vertx.currentContext().config()
def mongoClient = MongoClient.createShared(vertx, config.mongo)
def eventBus = vertx.eventBus()

eventBus.consumer "create.user", { message ->

    mongoClient.save "users", message.body(), { res ->

        if (res.succeeded())
            message.reply "[OK]"
        else
            message.fail 500, "[FAIL]"
    }
}