import io.vertx.core.AbstractVerticle
import io.vertx.core.DeploymentOptions
import io.vertx.core.json.JsonObject

class MainVerticle extends AbstractVerticle {

  @Override
  void start() {
    def config = vertx.currentContext().config()
    DeploymentOptions options = new DeploymentOptions().setConfig(new JsonObject(config))
    
    vertx.deployVerticle("src/main/groovy/EndPointVerticle.groovy", options)
    vertx.deployVerticle("src/main/groovy/MongoVerticle.groovy", options)
  }
}