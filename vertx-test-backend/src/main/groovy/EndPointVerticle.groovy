import io.vertx.groovy.ext.web.Router
import io.vertx.groovy.ext.web.handler.BodyHandler
import io.vertx.groovy.ext.web.handler.CorsHandler
import io.vertx.core.json.Json
import io.vertx.core.json.JsonObject
import io.vertx.core.http.HttpMethod

def server = vertx.createHttpServer()
def router = Router.router(vertx)
def eventBus = vertx.eventBus()

router.route().handler(CorsHandler.create("*")
    .allowedMethod(HttpMethod.GET)
    .allowedMethod(HttpMethod.POST)
    .allowedMethod(HttpMethod.OPTIONS)
    .allowedHeader("Access-Control-Request-Method")
    .allowedHeader("Access-Control-Allow-Credentials")
    .allowedHeader("Access-Control-Allow-Origin")
    .allowedHeader("Access-Control-Allow-Headers")
    .allowedHeader("Content-Type"));
router.route().handler BodyHandler.create()

router.route("/").handler { routingContext ->
    routingContext
        .response()
        .putHeader("content-type", "text/plain")
        .end "Hello World"
}

router.post("/user").handler { routingContext ->

    def data = Json.decodeValue routingContext.getBodyAsString(), Map.class
    
    eventBus.send "create.user", data, { reply ->

        def response = routingContext.response()
            .putHeader "content-type", "application/json; charset=utf-8"

        if (reply.succeeded())
            response.setStatusCode(201).end Json.encodePrettily([ msg: "OK" ])
        else
            response.setStatusCode(500).end Json.encodePrettily([ msg: "ERROR" ])
    }
}

server.requestHandler(router.&accept).listen 8080