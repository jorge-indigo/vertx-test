package mx.dsindigo.http.handlers;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

public class HelloWorldHandler implements Handler<RoutingContext> {

    public void handle(RoutingContext routingContext) {
        routingContext.response().end("Hello World!");
    }
}

