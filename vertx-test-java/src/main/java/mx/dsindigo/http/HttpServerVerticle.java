package mx.dsindigo.http;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import mx.dsindigo.http.handlers.*;

public class HttpServerVerticle extends AbstractVerticle {

    @Override
    public void start(Future<Void> startFuture) {
        HttpServer server = vertx.createHttpServer();

        Router router = Router.router(vertx);

        router.get("/").handler(new HelloWorldHandler());

        server.requestHandler(router::accept).listen(8080, res -> {
            if (res.succeeded()) {
                startFuture.complete();
            } else {
                startFuture.fail(res.cause());
            }
        });
    }
}