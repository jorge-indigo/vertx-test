package mx.dsindigo.net.handlers;

import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.net.NetSocket;
import io.vertx.core.Vertx;

public class BufferHandler implements Handler<Buffer> {

    private Vertx vertx;
    private NetSocket socket;

    public BufferHandler(Vertx vertx, NetSocket socket) {
        this.vertx = vertx;
        this.socket = socket;
    }

    @Override
    public void handle(Buffer buffer) {
        String [] input = new String(buffer.getBytes()).trim().split("\r\n");

        if (input.length == 0) { this.commandNotFound(); return; }

        String command = input[0];
        String [] commandParts = command.split(" ");

        if (commandParts.length == 0) { this.commandNotFound(); return; }

        switch(commandParts[0]) {
            case "undeploy": 
                try { this.undeploy(commandParts[1]); } 
                catch(Exception ex) { undeployError(); }
                break;
            case "deploy": 
                try { this.deploy(commandParts[1]); } 
                catch(Exception ex) { deployError(); }
                break;
            case "list": this.list(); break;
            case "quit": this.quit(); break;
            default: this.commandNotFound();
        }
    }

    private void write(String message) {
        this.socket.write(message + "\r\n\n> ");
    }

    private void commandNotFound() {
        this.write("[ERROR] Command not found!");
    }

    private void quit() {
        this.socket.close();
    }

    private void list() {
        for(String deploymentID : this.vertx.deploymentIDs()) {
            this.socket.write(deploymentID + "\r\n");
        }
        this.socket.write("\r\n> ");
    }

    private void deploy(String verticle) {
        this.socket.write("Deploying '" + verticle + "'...\n");
        vertx.deployVerticle(verticle, res -> {
            if (res.succeeded()) {
                this.write("Verticle ID: " + res.result());
            } else {
                this.write("[ERROR] Can't deploy verticle: \n" + res.cause());
            }
        });
    }

    private void undeploy(String deploymentID) {
        this.socket.write("Undeploying '" + deploymentID + "'...\n");
        vertx.undeploy(deploymentID, res -> {
            if (res.succeeded()) {
                this.write("Ok!");
            } else {
                this.write("[ERROR] Can't undeploy verticle: \n" + res.cause());
            }
        });
    }

    private void deployError() {
        this.write("[ERROR] Please specify the java class...");
    }

    private void undeployError() {
        this.write("[ERROR] Please specify deployment ID...");
    }
}