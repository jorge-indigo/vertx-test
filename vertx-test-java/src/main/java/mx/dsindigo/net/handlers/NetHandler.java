package mx.dsindigo.net.handlers;

import io.vertx.core.net.NetSocket;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;

public class NetHandler implements Handler<NetSocket> {

    private Vertx vertx;

    public NetHandler(Vertx vertx) {
        this.vertx = vertx;
    }

    @Override
    public void handle(NetSocket socket) {
        socket.write(this.welcome());
        socket.handler(new BufferHandler(vertx, socket));
    }

    private String welcome() {
        return 
        "# Vert.x PM2\r\n" +
        "# -------------------------------------------\r\n" +
        "# type 'help' for a list of available commands\r\n" +
        "# type 'quit' to exit\r\n" +
        "#\r\n" +
        "> ";
    }
}