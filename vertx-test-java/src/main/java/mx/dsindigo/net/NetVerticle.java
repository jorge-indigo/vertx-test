package mx.dsindigo.net;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.net.NetServer;
import mx.dsindigo.net.handlers.*;

public class NetVerticle extends AbstractVerticle {

    @Override
    public void start() {
        NetServer server = vertx.createNetServer();
        server.connectHandler(new NetHandler(vertx));
        server.listen(1234, "localhost");
    }
}